# Node Challange

### Installation

How to start the app on local machine:
```sh
$ nvm use 10.15.1
$ npm install
$ npm run build
$ npm run start 

```

### App structure
- src
  - controllers
  - helpers
  - models
  - routes
  - views

### Uses
- REST based APIs apps.


