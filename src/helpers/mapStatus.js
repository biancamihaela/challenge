import config from '../../config/default';
import {
  EN_ROUTE_TO_SENDER,
  WAITING_AT_SENDER,
  EN_ROUTE_TO_RECIPIENT,
  DELIVERED,
  UNSUCCESFUL,
  INFO_RECEIVED,
  IN_TRANSIT,
  OUT_FOR_DELIVERY,
  FAILED_ATTEMPT,
  PENDING,
} from '../constants/index';

// We could even do this if we knew the position of the statuses
// in the list did not change
// In the consignment controller we would just map the result
// and get the status we need for our order
const mapStatus = () => {
  const mappedStatus = [];
  config.consignmentStatuses.forEach((status, index) => {
    mappedStatus.push({ [status]: config.orderStatus[index] });
  });
  return mappedStatus;
};

const mapConsignmentStatusToOrderStatus = (status) => {
  let orderStatus;
  const consignmentStatus = status;
  switch (consignmentStatus) {
    case EN_ROUTE_TO_SENDER:
      orderStatus = INFO_RECEIVED;
      break;
    case WAITING_AT_SENDER:
      orderStatus = IN_TRANSIT;
      break;
    case EN_ROUTE_TO_RECIPIENT:
      orderStatus = OUT_FOR_DELIVERY;
      break;
    case DELIVERED:
      orderStatus = DELIVERED;
      break;
    case UNSUCCESFUL:
      orderStatus = FAILED_ATTEMPT;
      break;
    default:
      orderStatus = PENDING;
  }
  return orderStatus;
};

export default {
  mapConsignmentStatusToOrderStatus,
  mapStatus,
};
