import express from 'express';

import consignments from './consignments';
import orders from './orders';
import notification from './notification';

const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});

router.use('/api/v1/consignment', consignments);
router.use('/api/v1/order', orders);
router.use('/api/v1/notification', notification);

module.exports = router;
