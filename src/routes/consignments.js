import express from 'express';
import ConsignmentsController from '../controllers/Consignments';

const router = express.Router();

router.get('/:consignmentId', ConsignmentsController.getConsignmentById);
router.post('/consignment', ConsignmentsController.updateConsignment);

export default router;
