import express from 'express';
import OrdersController from '../controllers/Orders';

const router = express.Router();

router.post('/order', OrdersController.createOrder);
router.get('/:id', OrdersController.getOrderById);
router.get('/status/:id', OrdersController.getCurrentOrderStatusById);

export default router;
