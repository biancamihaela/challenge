import express from 'express';
import NotificationController from '../controllers/Notification';

const router = express.Router();

router.get('/send_notification/:userId/:orderId/:status', NotificationController.sendNotifcationToUser);

export default router;
