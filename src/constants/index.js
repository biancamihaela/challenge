// CONSIGNMENT STATUES
export const EN_ROUTE_TO_SENDER = 'en_route_to_sender';
export const WAITING_AT_SENDER = 'waiting_at_sender';
export const EN_ROUTE_TO_RECIPIENT = 'en_route_to_recipient';
export const DELIVERED = 'delivered';
export const UNSUCCESFUL = 'unsuccesful';

// ORDER STATUES
export const INFO_RECEIVED = 'info_received';
export const IN_TRANSIT = 'in_transit';
export const OUT_FOR_DELIVERY = 'out_for_delivery';
export const FAILED_ATTEMPT = 'failed_attempt';
export const PENDING = 'pending';
