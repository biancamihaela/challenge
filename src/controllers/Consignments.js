import Order from '../models/order';
import Consignment from '../models/consignment';
import mapStatusHelper from '../helpers/mapStatus';


const getConsignmentById = async (req, res) => {
  try {
    const { consignmentId } = req.params;
    const consignment = await Order.findOne({ consignmentId });

    if (consignment === null) {
      res.status(404);
      res.send({ error: 'Consignment not found' });
      return;
    }

    res.status(200);
    res.send(consignment);
  } catch (e) {
    res.status(400);
    res.send(e.message);
  }
};


const updateConsignment = async (req, res) => {
  try {
    const { consignmentId, coordinates, status } = req.body;
    const order = await Order.findOne({ consignmentId });

    if (!order || order === null) {
      res.status(404);
      res.send({ error: `There is no order for this consignment ${consignmentId}` });
      return;
    }

    const consignment = await Consignment.findOne({ consignmentId });

    let isCoordinateInConsignment;

    if (!consignment || consignment === null) {
      const newConsignment = await Consignment.createAndSave({ ...req.body });

      if (newConsignment.coordinates.length > 0) {
        isCoordinateInConsignment = newConsignment.coordinates
          .find((element, index) => element === coordinates[index]) !== undefined;
      }

      if ((newConsignment.status !== status) || (isCoordinateInConsignment === false)) {
        await Consignment.findOneAndUpdate({ consignmentId }, req.body, { new: true });

        const orderStatus = mapStatusHelper.mapConsignmentStatusToOrderStatus(status);

        await Order.findOneAndUpdate({ id: order.id }, { $set: { status: orderStatus } });

        res.status(200);
        res.send('Consignment updated successfully');
        return;
      }
      res.status(200);
      res.send('Consignment updated successfully');
      return;
    }

    if (consignment.coordinates.length > 0) {
      isCoordinateInConsignment = consignment.coordinates
        .find((element, index) => element === coordinates[index]) !== undefined;
    }

    if ((consignment.status !== status) || (isCoordinateInConsignment === false)) {
      await Consignment.findOneAndUpdate({ consignmentId }, req.body, { new: true });

      const orderStatus = mapStatusHelper.mapConsignmentStatusToOrderStatus(status);

      await Order.findOneAndUpdate({ id: order.id }, { $set: { status: orderStatus } });

      res.status(200);
      res.send('Consignment updated successfully');
      return;
    }

    res.status(200);
    res.send('No Change in status');
  } catch (e) {
    res.status(400);
    res.send(e.message);
  }
};

export default {
  getConsignmentById,
  updateConsignment,
};
