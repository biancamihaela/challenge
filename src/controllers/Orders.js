import Order from '../models/order';

const createOrder = async (req, res) => {
  try {
    const order = req.body;
    const savedOrder = await Order.createAndSave({ ...order });
    res.status(200);
    res.send(savedOrder);
  } catch (e) {
    res.status(400);
    res.send(e.message);
  }
};

const getOrderById = async (req, res) => {
  try {
    const { id } = req.params;
    const order = await Order.findOne({ id });

    if (!order || order === null) {
      res.status(404);
      res.send({ error: 'Order not found' });
      return;
    }

    res.status(200);
    res.send(order);
  } catch (e) {
    res.status(400);
    res.send(e.message);
  }
};

const getCurrentOrderStatusById = async (req, res) => {
  try {
    const { id } = req.params;

    const order = await Order.findOne({ id });

    if (!order || order === null) {
      res.status(404);
      res.send({ error: 'Order not found' });
      return;
    }
    const orderStatus = {
      id,
      status: order.status,
    };
    res.status(200);
    res.send(orderStatus);
  } catch (e) {
    res.status(400);
    res.send(e.message);
  }
};

export default {
  createOrder,
  getOrderById,
  getCurrentOrderStatusById,
};
