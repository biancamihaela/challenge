import Order from '../models/order';

const sendNotifcationToUser = async (req, res) => {
  try {
    const { userId, orderId, status } = req.params;
    const order = await Order.findOne({ id: orderId });

    if ((!order || order === null) && (order.userId !== userId)) {
      res.status(404);
      res.send({ error: 'Order not found' });
      return;
    }

    // Could use node mailer to send an email to the user
    const notification = {
      from: 'sender@sener.com',
      to: 'user@user.com',
      subject: `Order ${orderId} status update`,
      text: `Your new order status is ${status}`,
    };

    res.status(200);
    res.send(notification);
  } catch (e) {
    res.status(400);
    res.send(e.message);
  }
};

export default {
  sendNotifcationToUser,
};
