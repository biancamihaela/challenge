import mongoose from 'mongoose';

const { Schema } = mongoose;

const ConsignmentSchema = new Schema({
  consignmentId: {
    type: Number,
    required: true,
  },
  coordinates: {
    type: [Number, Number],
  },
  status: {
    type: String,
    default: '',
  },
});

// eslint-disable-next-line func-names
ConsignmentSchema.statics.createAndSave = async function (record) {
  try {
    const newRecord = new this(record);
    return await newRecord.save();
  } catch (e) {
    return e;
  }
};

export default mongoose.model('consignments', ConsignmentSchema);
