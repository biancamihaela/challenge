import mongoose from 'mongoose';

const { Schema } = mongoose;

const OrderSchema = new Schema({
  id: {
    type: Number,
    required: true,
  },
  userId: {
    type: Number,
    required: true,
  },
  consignmentId: {
    type: Number,
    required: true,
  },
  deliveryLocation: {
    type: [Number, Number],
  },
  senderLocation: {
    type: [Number, Number],
  },
  status: {
    type: String,
  },
});

// eslint-disable-next-line func-names
OrderSchema.statics.createAndSave = async function (record) {
  try {
    const newRecord = new this(record);
    return await newRecord.save();
  } catch (e) {
    return e;
  }
};

export default mongoose.model('orders', OrderSchema);
