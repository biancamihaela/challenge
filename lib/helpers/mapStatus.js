'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _default = require('../../config/default');

var _default2 = _interopRequireDefault(_default);

var _index = require('../constants/index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// We could even do this if we knew the position of the statuses
// in the list did not change
// In the consignment controller we would just map the result
// and get the status we need for our order
var mapStatus = function mapStatus() {
  var mappedStatus = [];
  _default2.default.consignmentStatuses.forEach(function (status, index) {
    mappedStatus.push(_defineProperty({}, status, _default2.default.orderStatus[index]));
  });
  return mappedStatus;
};

var mapConsignmentStatusToOrderStatus = function mapConsignmentStatusToOrderStatus(status) {
  var orderStatus = void 0;
  var consignmentStatus = status;
  switch (consignmentStatus) {
    case _index.EN_ROUTE_TO_SENDER:
      orderStatus = _index.INFO_RECEIVED;
      break;
    case _index.WAITING_AT_SENDER:
      orderStatus = _index.IN_TRANSIT;
      break;
    case _index.EN_ROUTE_TO_RECIPIENT:
      orderStatus = _index.OUT_FOR_DELIVERY;
      break;
    case _index.DELIVERED:
      orderStatus = _index.DELIVERED;
      break;
    case _index.UNSUCCESFUL:
      orderStatus = _index.FAILED_ATTEMPT;
      break;
    default:
      orderStatus = _index.PENDING;
  }
  return orderStatus;
};

exports.default = {
  mapConsignmentStatusToOrderStatus: mapConsignmentStatusToOrderStatus,
  mapStatus: mapStatus
};
module.exports = exports['default'];
//# sourceMappingURL=mapStatus.js.map