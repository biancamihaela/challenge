'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
// CONSIGNMENT STATUES
var EN_ROUTE_TO_SENDER = exports.EN_ROUTE_TO_SENDER = 'en_route_to_sender';
var WAITING_AT_SENDER = exports.WAITING_AT_SENDER = 'waiting_at_sender';
var EN_ROUTE_TO_RECIPIENT = exports.EN_ROUTE_TO_RECIPIENT = 'en_route_to_recipient';
var DELIVERED = exports.DELIVERED = 'delivered';
var UNSUCCESFUL = exports.UNSUCCESFUL = 'unsuccesful';

// ORDER STATUES
var INFO_RECEIVED = exports.INFO_RECEIVED = 'info_received';
var IN_TRANSIT = exports.IN_TRANSIT = 'in_transit';
var OUT_FOR_DELIVERY = exports.OUT_FOR_DELIVERY = 'out_for_delivery';
var FAILED_ATTEMPT = exports.FAILED_ATTEMPT = 'failed_attempt';
var PENDING = exports.PENDING = 'pending';
//# sourceMappingURL=index.js.map