'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _order = require('../models/order');

var _order2 = _interopRequireDefault(_order);

var _consignment = require('../models/consignment');

var _consignment2 = _interopRequireDefault(_consignment);

var _mapStatus = require('../helpers/mapStatus');

var _mapStatus2 = _interopRequireDefault(_mapStatus);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var getConsignmentById = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var consignmentId, consignment;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            consignmentId = req.params.consignmentId;
            _context.next = 4;
            return _order2.default.findOne({ consignmentId: consignmentId });

          case 4:
            consignment = _context.sent;

            if (!(consignment === null)) {
              _context.next = 9;
              break;
            }

            res.status(404);
            res.send({ error: 'Consignment not found' });
            return _context.abrupt('return');

          case 9:

            res.status(200);
            res.send(consignment);
            _context.next = 17;
            break;

          case 13:
            _context.prev = 13;
            _context.t0 = _context['catch'](0);

            res.status(400);
            res.send(_context.t0.message);

          case 17:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 13]]);
  }));

  return function getConsignmentById(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var updateConsignment = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var _req$body, consignmentId, coordinates, status, order, consignment, isCoordinateInConsignment, newConsignment, orderStatus, _orderStatus;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _req$body = req.body, consignmentId = _req$body.consignmentId, coordinates = _req$body.coordinates, status = _req$body.status;
            _context2.next = 4;
            return _order2.default.findOne({ consignmentId: consignmentId });

          case 4:
            order = _context2.sent;

            if (!(!order || order === null)) {
              _context2.next = 9;
              break;
            }

            res.status(404);
            res.send({ error: 'There is no order for this consignment ' + consignmentId });
            return _context2.abrupt('return');

          case 9:
            _context2.next = 11;
            return _consignment2.default.findOne({ consignmentId: consignmentId });

          case 11:
            consignment = _context2.sent;
            isCoordinateInConsignment = void 0;

            if (!(!consignment || consignment === null)) {
              _context2.next = 30;
              break;
            }

            _context2.next = 16;
            return _consignment2.default.createAndSave(_extends({}, req.body));

          case 16:
            newConsignment = _context2.sent;

            if (newConsignment.coordinates.length > 0) {
              isCoordinateInConsignment = newConsignment.coordinates.find(function (element, index) {
                return element === coordinates[index];
              }) !== undefined;
            }

            if (!(newConsignment.status !== status || isCoordinateInConsignment === false)) {
              _context2.next = 27;
              break;
            }

            _context2.next = 21;
            return _consignment2.default.findOneAndUpdate({ consignmentId: consignmentId }, req.body, { new: true });

          case 21:
            orderStatus = _mapStatus2.default.mapConsignmentStatusToOrderStatus(status);
            _context2.next = 24;
            return _order2.default.findOneAndUpdate({ id: order.id }, { $set: { status: orderStatus } });

          case 24:

            res.status(200);
            res.send('Consignment updated successfully');
            return _context2.abrupt('return');

          case 27:
            res.status(200);
            res.send('Consignment updated successfully');
            return _context2.abrupt('return');

          case 30:

            if (consignment.coordinates.length > 0) {
              isCoordinateInConsignment = consignment.coordinates.find(function (element, index) {
                return element === coordinates[index];
              }) !== undefined;
            }

            if (!(consignment.status !== status || isCoordinateInConsignment === false)) {
              _context2.next = 40;
              break;
            }

            _context2.next = 34;
            return _consignment2.default.findOneAndUpdate({ consignmentId: consignmentId }, req.body, { new: true });

          case 34:
            _orderStatus = _mapStatus2.default.mapConsignmentStatusToOrderStatus(status);
            _context2.next = 37;
            return _order2.default.findOneAndUpdate({ id: order.id }, { $set: { status: _orderStatus } });

          case 37:

            res.status(200);
            res.send('Consignment updated successfully');
            return _context2.abrupt('return');

          case 40:

            res.status(200);
            res.send('No Change in status');
            _context2.next = 48;
            break;

          case 44:
            _context2.prev = 44;
            _context2.t0 = _context2['catch'](0);

            res.status(400);
            res.send(_context2.t0.message);

          case 48:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined, [[0, 44]]);
  }));

  return function updateConsignment(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

exports.default = {
  getConsignmentById: getConsignmentById,
  updateConsignment: updateConsignment
};
module.exports = exports['default'];
//# sourceMappingURL=Consignments.js.map