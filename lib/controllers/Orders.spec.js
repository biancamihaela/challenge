'use strict';

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var sinon = require('sinon');
var proxyquire = require('proxyquire');

var order = {
  id: 123,
  userId: 456,
  consignmentId: 789,
  deliveryLocation: [1233, 2222],
  senderLocation: [1111, 5555],
  status: 'in_transit'
};

describe('Controllers/Orders', function () {
  describe('GET/ Order By ID', function () {
    it('should return the order by ID', _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var id, req, res, OrderSchemaPromise, OrderSchemaStub, OrderCtl;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              id = 123;
              req = sinon.spy();

              req.params = id;

              res = {
                status: sinon.spy(),
                send: sinon.stub()
              };
              OrderSchemaPromise = Promise.resolve(order);
              OrderSchemaStub = sinon.stub().returns(OrderSchemaPromise);
              OrderCtl = proxyquire('./Orders', {
                '../models/order': {
                  findOne: OrderSchemaStub
                }
              });
              _context.next = 9;
              return OrderCtl.getOrderById(req, res);

            case 9:
              sinon.assert.calledWith(res.status, 200);

            case 10:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, undefined);
    })));
  });
});
//# sourceMappingURL=Orders.spec.js.map