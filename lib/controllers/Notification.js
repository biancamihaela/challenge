'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _order = require('../models/order');

var _order2 = _interopRequireDefault(_order);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var sendNotifcationToUser = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var _req$params, userId, orderId, status, order, notification;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _req$params = req.params, userId = _req$params.userId, orderId = _req$params.orderId, status = _req$params.status;
            _context.next = 4;
            return _order2.default.findOne({ id: orderId });

          case 4:
            order = _context.sent;

            if (!((!order || order === null) && order.userId !== userId)) {
              _context.next = 9;
              break;
            }

            res.status(404);
            res.send({ error: 'Order not found' });
            return _context.abrupt('return');

          case 9:

            // Could use node mailer to send an email to the user
            notification = {
              from: 'sender@sener.com',
              to: 'user@user.com',
              subject: 'Order ' + orderId + ' status update',
              text: 'Your new order status is ' + status
            };


            res.status(200);
            res.send(notification);
            _context.next = 18;
            break;

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            res.status(400);
            res.send(_context.t0.message);

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 14]]);
  }));

  return function sendNotifcationToUser(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.default = {
  sendNotifcationToUser: sendNotifcationToUser
};
module.exports = exports['default'];
//# sourceMappingURL=Notification.js.map