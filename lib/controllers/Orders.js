'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _order = require('../models/order');

var _order2 = _interopRequireDefault(_order);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var createOrder = function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var order, savedOrder;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            order = req.body;
            _context.next = 4;
            return _order2.default.createAndSave(_extends({}, order));

          case 4:
            savedOrder = _context.sent;

            res.status(200);
            res.send(savedOrder);
            _context.next = 13;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context['catch'](0);

            res.status(400);
            res.send(_context.t0.message);

          case 13:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, undefined, [[0, 9]]);
  }));

  return function createOrder(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

var getOrderById = function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var id, order;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            id = req.params.id;
            _context2.next = 4;
            return _order2.default.findOne({ id: id });

          case 4:
            order = _context2.sent;

            if (!(!order || order === null)) {
              _context2.next = 9;
              break;
            }

            res.status(404);
            res.send({ error: 'Order not found' });
            return _context2.abrupt('return');

          case 9:

            res.status(200);
            res.send(order);
            _context2.next = 17;
            break;

          case 13:
            _context2.prev = 13;
            _context2.t0 = _context2['catch'](0);

            res.status(400);
            res.send(_context2.t0.message);

          case 17:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, undefined, [[0, 13]]);
  }));

  return function getOrderById(_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}();

var getCurrentOrderStatusById = function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var id, order, orderStatus;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            id = req.params.id;
            _context3.next = 4;
            return _order2.default.findOne({ id: id });

          case 4:
            order = _context3.sent;

            if (!(!order || order === null)) {
              _context3.next = 9;
              break;
            }

            res.status(404);
            res.send({ error: 'Order not found' });
            return _context3.abrupt('return');

          case 9:
            orderStatus = {
              id: id,
              status: order.status
            };

            res.status(200);
            res.send(orderStatus);
            _context3.next = 18;
            break;

          case 14:
            _context3.prev = 14;
            _context3.t0 = _context3['catch'](0);

            res.status(400);
            res.send(_context3.t0.message);

          case 18:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined, [[0, 14]]);
  }));

  return function getCurrentOrderStatusById(_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}();

exports.default = {
  createOrder: createOrder,
  getOrderById: getOrderById,
  getCurrentOrderStatusById: getCurrentOrderStatusById
};
module.exports = exports['default'];
//# sourceMappingURL=Orders.js.map