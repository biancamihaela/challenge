'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _Consignments = require('../controllers/Consignments');

var _Consignments2 = _interopRequireDefault(_Consignments);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/:consignmentId', _Consignments2.default.getConsignmentById);
router.post('/consignment', _Consignments2.default.updateConsignment);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=consignments.js.map