'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _consignments = require('./consignments');

var _consignments2 = _interopRequireDefault(_consignments);

var _orders = require('./orders');

var _orders2 = _interopRequireDefault(_orders);

var _notification = require('./notification');

var _notification2 = _interopRequireDefault(_notification);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/', function (req, res) {
  res.render('index', { title: 'Express' });
});

router.use('/api/v1/consignment', _consignments2.default);
router.use('/api/v1/order', _orders2.default);
router.use('/api/v1/notification', _notification2.default);

module.exports = router;
//# sourceMappingURL=index.js.map