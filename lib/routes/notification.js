'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _Notification = require('../controllers/Notification');

var _Notification2 = _interopRequireDefault(_Notification);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.get('/send_notification/:userId/:orderId/:status', _Notification2.default.sendNotifcationToUser);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=notification.js.map