'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _Orders = require('../controllers/Orders');

var _Orders2 = _interopRequireDefault(_Orders);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();

router.post('/order', _Orders2.default.createOrder);
router.get('/:id', _Orders2.default.getOrderById);
router.get('/status/:id', _Orders2.default.getCurrentOrderStatusById);

exports.default = router;
module.exports = exports['default'];
//# sourceMappingURL=orders.js.map