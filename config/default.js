module.exports = {
  port: 3100,
  consignmentStatuses: [
    'en_route_to_sender',
    'waiting_at_sender',
    'en_route_to_recipient',
    'delivered',
    'unsuccesful',
  ],
  orderStatus: [
    'info_received',
    'in_transit',
    'out_for_delivery',
    'delivered',
    'failed_attempt',
  ],
  db: {
    url: 'mongodb://localhost/floomTest',
    options: {},
  },
};
